# siv

Command-line client for Zif.

## Usage

```
siv [host:port Or $ZIFNODE] <command> <args...>

Commands:
	help, h	Shows usage information.
	version, ver, V	Prints the version number.
	bootstrap, boot	Bootstraps to the given node.
	index, ind	Generates an FTS index.
	popular, pl	Fetches the n most popular posts.
	recent, rc	Fetches the n most recent posts.
	resolve, res	Resolves a zif address.
	addpost, add	Adds a post.
	search, s	Searches for posts.
	addmeta, addm, am	Adds a metadata key/value pair to a specified post.
	savecollection, savecoll, scoll	Saves the collection to disk.
	rebuildcollection, rebuildcoll, rbcoll	Rebuilds the collection from disk.
	peers	Lists all peers.
	suggest, sugg	Returns a list of suggested post titles for a given query.
	rap, raddpeer, reqaddpeer	Connect to the given peer
	set, s	Sets a key to a value
	get, g	Gets the value of a key-value pair.
	explore, expl	Start exploring for new peers.
	encode, enc	Encodes a raw address
	searchentry, se	Searches for an entry, given a name or description.
	profilecpu, profile-cpu, pcpu	Queries for CPU profiling information.
	profilemem, profile-mem, pmem	Queries for RAM profiling information.
	seedleech, sl	Sets the seed/leech info of a post.
	ping	Pings a peer.
	announce, ann	Announces to a peer.
	mirror, mir	Mirrors a peer.
	mprog, mp	Displays the progress of mirroring a peer.
	prsearch, prs	Performs a remote search on a peer.
	psearch, psr	Performs a search on a peer.
	precentc, prec	Lists a peer's recent posts.
	ppopular, pp	Lists a peer's popular posts.
	pindex, pind	Generates a peer's FTS index.
```

## Compiling

```sh
stack build --ghc-options -j$(nproc)
```

You can also `stack install` afterwards, if you want to.

