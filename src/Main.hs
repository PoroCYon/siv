{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.List          (find             )
import System.Environment (getArgs          )
import System.IO          (hPutStrLn, stderr)

import Siv.Address
import Siv.Command
import Siv.Commands.Peer
import Siv.Commands.Self
import Siv.ResultTypes

commands :: [Command]
commands = [helpc, versionc,

            bootstrapc, indexc, popularc, recentc, resolvec, addpostc,
            searchc, addmetac, {-getmetac,-} savecollc, rebuildcollc,
            peersc, suggestc, reqaddpeerc, setc, getc, explorec,
            encodec, searchentryc, profilecpuc, profilememc, seedleechc,

            pingc, announcec, mirrorc, mirrorprogc, prsearchc,
            psearchc, precentc, ppopularc, pindexc]

doCmd :: PubAddress -> Command -> [String] -> IO ()
doCmd a c args = (exec c) a commands args
doHelp :: IO ()
doHelp = doCmd defaddr helpc []
  where
    defaddr = PubAddress { host = "localhost", port = 8080 }

cmdOfVerb :: String -> Maybe Command
cmdOfVerb v = find (\ c -> elem (dropWhile ((==) '-') v) (names c)) commands

errText :: String
errText =
    "Please give the address and port of the node to use, either by supplying it as\n"
     ++ "the first argument, or by setting the ZIFNODE environment variable."


argparse :: [String] -> IO ()
argparse (url:vrb :rest) | elem ':' url =
    case (parse url, cmdOfVerb vrb) of
        (Just a, Just c) -> doCmd a c rest
        _ -> hPutStrLn stderr errText
argparse (vrb:rest) = do
    ea <- getEnvAddr
    case (ea, cmdOfVerb vrb) of
        (Just  a, Just c) -> doCmd a c rest
        (Nothing, Just _) -> hPutStrLn stderr errText
        _ -> doHelp
argparse _ = doHelp

main :: IO ()
main = getArgs >>= argparse

