{-# LANGUAGE OverloadedStrings #-}
module Siv.Address
    ( PubAddress(..)
    , ZifAddress
    , parse
    , getEnvAddr
    ) where

import Control.Exception   (try       )
import Data.Aeson          ((.:), (.=))
import Data.Aeson.Types    (FromJSON(..), ToJSON(..), Value(..), Parser(..))
import Data.List.Split     (splitOn   )
import Data.HashMap.Strict (fromList  )
import System.Environment  (getEnv    )
import Text.Read           (readMaybe )

import Siv.BaseByteString

import qualified Data.ByteString.Char8 as C8

data PubAddress = PubAddress { host :: String,
                               port :: Int   }
    deriving (Eq)
instance Show PubAddress where
    show pa = host pa ++ ":" ++ show (port pa)

newtype ZifAddress = Zif Base58ByteString
    deriving (Eq)

zifToB58 :: ZifAddress -> Base58ByteString
zifToB58 (Zif bs) = bs

b58ToZif :: Base58ByteString -> ZifAddress
b58ToZif bs = Zif bs

instance FromJSON ZifAddress where
    parseJSON (Object o) = do
        bs <- o .: "Raw" >>= parseJSON :: Parser Base64ByteString
        return $ Zif $ ofBS $ toBS bs
    parseJSON         j  = fail $ "Expected ZifAddress object, but got " ++ show j
instance ToJSON ZifAddress where
    toJSON (Zif bs) = Object $ fromList [("Raw", toJSON bs)]
instance Show ZifAddress where
    show (Zif bs) = "Z" ++ show bs
instance Read ZifAddress where
    readsPrec _ ('Z':bs) =
        case decodeBS $ C8.pack bs of
            Just b58s -> [(Zif b58s, "")]
            _         -> []
    readsPrec _ _ = []

parse :: String -> Maybe PubAddress
parse s | elem ':' s =
    let l = splitOn ":" s in
    if length l == 2
    then let h = head l
         in fmap (\ p -> PubAddress { host = h, port = p }) $ readMaybe $ head $ tail l
    else Nothing
parse _ = Nothing

getEnvAddr :: IO (Maybe PubAddress)
getEnvAddr = do
    nodea <- try $ getEnv "ZIFNODE" :: IO (Either IOError String)
    case nodea of
        Right a -> return $ parse a
        _       -> return   Nothing

