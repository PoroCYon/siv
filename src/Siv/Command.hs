module Siv.Command
    ( Command(..), CmdExecFn
    , helpc, versionc
    ) where

import Data.List          (intercalate    )
import System.Environment (getProgName    )
import System.IO          (hPutStr, stderr)

import Siv.Address

type CmdExecFn = PubAddress -> [Command] -> [String] -> IO ()
data Command = Command { names :: [String]  ,
                         help  ::  String   ,
                         exec  :: CmdExecFn }

helpc = Command { names = ["help", "h"] ,
                  help  = "Shows usage information." ,
                  exec  = execc                      }
  where
    execc _ commands _ = do
        pn <- getProgName
        hPutStr stderr $ pn ++ " [host:port Or $ZIFNODE] <command> <args...>\n"
            ++ "\n"
            ++ "Commands:\n"
            ++ foldl (++) [] (map (\c -> "\t" ++ intercalate ", " (names c) ++ "\t" ++ help c ++ "\n") commands)

versionc = Command { names = ["version", "ver", "V"]      ,
                     help  = "Prints the version number." ,
                     exec  = execc                        }
  where
    package = "siv"
    version = "0.1.0.0"
    execc _ _ _ = putStrLn $ package ++ " " ++ version

