{-# LANGUAGE OverloadedStrings, FlexibleInstances #-}
module Siv.BaseByteString
    ( BaseByteString(..)
    , Base58ByteString(..)
    , Base64ByteString(..)
    ) where

import Data.Aeson.Types (FromJSON(..), ToJSON(..), Parser(..), Value(..))
import Data.ByteString  (ByteString)
import Data.Text        (Text      )

import Data.ByteString        as BS
import Data.ByteString.Base58 as B58
import Data.ByteString.Base64 as B64
import Data.ByteString.Char8  as C8
import Data.Text              as T

class Eq a => BaseByteString a where
    encodeBS :: a -> ByteString
    decodeBS :: ByteString -> Maybe a
    toBS :: a -> ByteString
    ofBS :: ByteString -> a

newtype Base58ByteString = B58BS ByteString
    deriving (Eq)
newtype Base64ByteString = B64BS ByteString
    deriving (Eq)

instance BaseByteString Base58ByteString where
    encodeBS (B58BS bs) = B58.encodeBase58 B58.bitcoinAlphabet bs
    decodeBS bs = fmap B58BS $ B58.decodeBase58 B58.bitcoinAlphabet bs

    toBS (B58BS bs) = bs
    ofBS = B58BS

instance BaseByteString Base64ByteString where
    encodeBS (B64BS bs) = B64.encode bs
    decodeBS bs =
        case B64.decode bs of
            Right bs -> Just $ B64BS bs
            _        -> Nothing

    toBS (B64BS bs) = bs
    ofBS = B64BS

instance Show Base58ByteString where
    show bs = C8.unpack $ encodeBS bs
instance FromJSON Base58ByteString where
    parseJSON (String t) =
        case decodeBS $ C8.pack $ T.unpack t of
            Just bs -> return bs
            _       -> fail $ "Cannot decode base-58 string " ++ T.unpack t
    parseJSON         j  = fail $ "Expected a base-58 string, but got " ++ show j
instance ToJSON Base58ByteString where
    toJSON bs = String $ T.pack $ C8.unpack $ encodeBS bs

instance Show Base64ByteString where
    show bs = C8.unpack $ encodeBS bs
instance FromJSON Base64ByteString where
    parseJSON (String t) =
        case decodeBS $ C8.pack $ T.unpack t of
            Just bs -> return bs
            _       -> fail $ "Cannot decode base-64 string." ++ T.unpack t
    parseJSON         j  = fail $ "Expected a base-64 string, but got " ++ show j
instance ToJSON Base64ByteString where
    toJSON bs = String $ T.pack $ C8.unpack $ encodeBS bs

