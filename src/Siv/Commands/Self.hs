{-# LANGUAGE OverloadedStrings #-}
module Siv.Commands.Self
    ( bootstrapc
    , indexc
    , popularc
    , recentc
    , resolvec
    , addpostc
    , searchc
    , addmetac
  --, getmetac
    , savecollc
    , rebuildcollc
    , peersc
    , suggestc
    , reqaddpeerc
    , setc
    , getc
    , explorec
    , encodec
    , searchentryc
    , profilecpuc
    , profilememc
    , seedleechc
    ) where

import Data.Aeson (toJSON           )
import Data.List  (intercalate      )
import Data.Maybe (isJust           )
import Data.Text  (Text             )
import System.IO  (hPutStrLn, stderr)
import Text.Read  (readMaybe        )

import Siv.Address
import Siv.BaseByteString
import Siv.Command
import Siv.ResultTypes
import Siv.Util

import qualified Data.ByteString.Char8 as C8
import qualified Data.Text             as T

isPeer :: String -> Bool
isPeer ('Z':rest) = isJust (decodeBS $ C8.pack rest :: Maybe Base58ByteString)
isPeer   _        = False

isInt :: String -> Bool
isInt s = isJust (readMaybe s :: Maybe Int)

bootstrapc :: Command
bootstrapc =  Command { names = [ "bootstrap", "boot"         ],
                        help  = "Bootstraps to the given node.",
                        exec  = execc                          }
  where
    execc :: CmdExecFn
    execc pa _ (a:_) | isHost a = do
        r <- httpResult (doGetRequest pa $ "/self/bootstrap/" ++ a ++ "/") :: IO HttpResult
        putStrLn $ show r
    execc _  _ _ =
        hPutStrLn stderr $ "Error: host (and port) needed,\n"
                            ++ "formatted as \"host\" or \"host:port\"."

indexc :: Command
indexc =  Command { names = [ "index", "ind"        ],
                    help  = "Generates an FTS index.",
                    exec  = execc                    }
  where
    execc :: CmdExecFn
    execc pa _ (i:_) | isInt i = do
        r <- httpResult (doGetRequest pa $ "/self/index/" ++ i ++ "/") :: IO HttpResult
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Error: integer argument needed."

popularc :: Command
popularc =  Command { names = [ "popular", "pl"                 ],
                      help  = "Fetches the n most popular posts.",
                      exec  = execc                              }
  where
    execc :: CmdExecFn
    execc pa _ (n:_) | isInt n = do
        r <- httpResult (doGetRequest pa $ "/self/popular/" ++ n ++ "/") :: IO (HttpRes [ZifPost])
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Error: amount of popular posts to fetch expected."

recentc :: Command
recentc =  Command { names = [ "recent", "rc"                 ],
                     help  = "Fetches the n most recent posts.",
                     exec  = execc                             }
  where
    execc :: CmdExecFn
    execc pa _ (n:_) | isInt n = do
        r <- httpResult (doGetRequest pa $ "/self/recent/" ++ n ++ "/") :: IO (HttpRes [ZifPost])
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Error: amount of recent posts to fetch expected."

resolvec :: Command
resolvec =  Command { names = [ "resolve", "res"      ],
                      help  = "Resolves a zif address.",
                      exec  = execc                    }
  where
    execc :: CmdExecFn
    execc pa _ (a:_) | isPeer a = do
        r <- httpResult (doGetRequest pa $ "/self/resolve/" ++ a ++ "/") :: IO (HttpRes ZifEntry)
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Error: expected address to resolve."

addpostc :: Command
addpostc =  Command { names = [ "addpost", "add" ],
                      help  = "Adds a post."      ,
                      exec  = execc               }
  where
    toInt  s = readMaybe s :: Maybe Int
    toLong s = readMaybe s :: Maybe Integer
    isLong s = isJust $ toLong s
    correctFormat id _ _ sz fc ss ls ud _ =
        isInt id && isInt sz && isInt fc && isInt ss && isInt ls && isLong ud
    errMsg = "Error: expected post ID, InfoHash, title, size, file count, seeders, leechers, upload date and tags."
    mkPost id ih tt sz fc ss ls ud ts = do
        id <- toInt id
        sz <- toInt sz
        fc <- toInt fc
        ss <- toInt ss
        ls <- toInt ls
        ud <- toLong ud
        return $ ZifPost { postid = id, infoHash = T.pack ih, title = T.pack tt,
                           size = sz, fileCount = fc, seeders = ss,
                           leechers = ls, uploadDate = ud, tags = T.pack ts }
    execc :: CmdExecFn
    execc pa _ (id:ih:tt:sz:fc:ss:ls:ud:ts:_) | correctFormat id ih tt sz fc ss ls ud ts =
        case mkPost id ih tt sz fc ss ls ud ts of
            Just po -> do
                r <- httpResult (doPostRequest pa "/self/addpost/" [("data", encodeJSON $ toJSON po)]) :: IO HttpResult
                print $ show r
            _ -> hPutStrLn stderr errMsg
    execc _  _  _ =
        hPutStrLn stderr errMsg
searchc :: Command
searchc =  Command { names = [ "search", "s"     ],
                     help  = "Searches for posts.",
                     exec  = execc                }
  where
    execc :: CmdExecFn
    execc pa _ (p:x:xs) | isInt p = do
        let f = [("query", C8.pack $ intercalate " " (x:xs)),
                 ("page" , C8.pack p                       )]
        r <- httpResult (doPostRequest pa "/self/search/" f) :: IO (HttpRes SearchList)
        print $ show r
    execc pa _ _ = hPutStrLn stderr $ "Expected page number & search string."

addmetac :: Command
addmetac =  Command { names = [ "addmeta", "addm", "am"                           ],
                      help  = "Adds a metadata key/value pair to a specified post.",
                      exec  = execc                                                }
  where
    execc :: CmdExecFn
    execc pa _ (i:k:v:_) | isInt i = do
        r <- httpResult (doGetRequest pa $ "/self/addmeta/" ++ i ++ "/" ++ k ++ "/" ++ v ++ "/") :: IO HttpResult
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Error: expected post ID, metadata key and value."

{-getmetac :: Command
getmetac =  Command { names = [ "getmeta", "getm", "gm"                                  ],
                      help  = "Retreives a metadata key/value pair from a specified post.",
                      exec  = execc                                                       }
  where
    execc :: CmdExecFn
    execc pa _ (i:k:_) | isInt i = do
        r <- httpResult (doGetRequest pa $ "/self/getmeta/" ++ i ++ "/" ++ k ++ "/") :: IO (HttpRes Text)
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Error: expected post ID and a metadata key."-}

savecollc :: Command
savecollc =  Command { names = [ "savecollection", "savecoll", "scoll" ],
                       help  = "Saves the collection to disk."          ,
                       exec  = execc                                    }
  where
    execc :: CmdExecFn
    execc pa _ _ = do
        r <- httpResult (doGetRequest pa $ "/self/savecollection/") :: IO HttpResult
        putStrLn $ show r

rebuildcollc :: Command
rebuildcollc =  Command { names = [ "rebuildcollection", "rebuildcoll", "rbcoll" ],
                          help  = "Rebuilds the collection from disk."            ,
                          exec  = execc                                           }
  where
    execc :: CmdExecFn
    execc pa _ _ = do
        r <- httpResult (doGetRequest pa $ "/self/rebuildcollection/") :: IO HttpResult
        putStrLn $ show r

peersc :: Command
peersc =  Command { names = [ "peers"        ],
                    help  = "Lists all peers.",
                    exec  = execc             }
  where
    execc :: CmdExecFn
    execc pa _ _ = do
        r <- httpResult (doGetRequest pa $ "/self/peers/") :: IO (HttpRes [ZifEntry])
        putStrLn $ show r

suggestc :: Command
suggestc =  Command { names = [ "suggest", "sugg"                                        ],
                      help  = "Returns a list of suggested post titles for a given query.",
                      exec  = execc                                                       }
  where
    execc :: CmdExecFn
    execc pa _ (q:qs) = do
        let f = [("query", C8.pack $ intercalate " " (q:qs))]
        r <- httpResult (doPostRequest pa "/self/suggest/" f) :: IO (HttpRes [String])
        putStrLn $ show r
    execc _  _ _ = hPutStrLn stderr $ "Expected search query."

reqaddpeerc :: Command
reqaddpeerc =  Command { names = [ "rap", "raddpeer", "reqaddpeer" ],
                         help  = "Connect to the given peer"        ,
                         exec  = execc                              }
  where
    execc :: CmdExecFn
    execc pa _ (r:p:_) | isPeer p = do
        r <- httpResult (doGetRequest pa $ "/self/requestaddpeer/" ++ r ++ ('/':p) ++ "/") :: IO HttpResult
        putStrLn $ show r
    execc pa _  _ = hPutStrLn stderr $ "Expected remote and peer."

setc :: Command
setc =  Command { names = [ "set", "s"          ],
                  help  = "Sets a key to a value",
                  exec  = execc                  }
  where
    execc :: CmdExecFn
    execc pa _ (k:v:vs) = do
        let f = [("value", C8.pack $ intercalate " " $ v:vs)]
        r <- httpResult (doPostRequest pa ("/self/set/" ++ k ++ "/") f) :: IO HttpResult
        putStrLn $ show r
    execc pa _  _ = hPutStrLn stderr $ "Expected key and value."

getc :: Command
getc =  Command { names = [ "get", "g"                        ],
                  help  = "Gets the value of a key-value pair.",
                  exec  = execc                                }
  where
    execc :: CmdExecFn
    execc pa _ (k:_) = do
        r <- httpResult (doGetRequest pa $ "/self/get/" ++ k ++ "/") :: IO (HttpRes String)
        putStrLn $ show r
    execc pa _  _ = hPutStrLn stderr $ "Expected key."

explorec :: Command
explorec =  Command { names = [ "explore", "expl"            ],
                      help  = "Start exploring for new peers.",
                      exec  = execc                           }
  where
    execc :: CmdExecFn
    execc pa _ _ = do
        r <- httpResult (doGetRequest pa $ "/self/explore/") :: IO HttpResult
        putStrLn $ show r

encodec :: Command
encodec =  Command { names = [ "encode", "enc"     ],
                     help  = "Encodes a raw address",
                     exec  = execc                  }
  where
    execc :: CmdExecFn
    execc pa _ (r:_) | isJust (decodeBS (C8.pack r) :: Maybe Base64ByteString) = do
        let f = [("raw", C8.pack r)]
        r <- httpResult (doPostRequest pa "/self/encode/" f) :: IO (HttpRes String)
        putStrLn $ show r
    execc _  _  _ = hPutStrLn stderr $ "Expected a base64-encoded raw address."

searchentryc :: Command
searchentryc =  Command { names = [ "searchentry", "se"                     ],
                          help  = "Searches for an entry, given a name or description.",
                          exec  = execc                                      }
  where
    execc :: CmdExecFn
    execc pa _ (p:n:d) = do
        let f = [("name", C8.pack n),
                 ("page", C8.pack p),
                 ("desc", C8.pack $ intercalate " " d)]
        r <- httpResult (doPostRequest pa "/self/searchentry/" f) :: IO (HttpRes [ZifEntry])
        putStrLn $ show r
    execc _ _ _ = hPutStrLn stderr $ "Expected a page, name and description."

profilecpuc :: Command
profilecpuc =  Command { names = ["profilecpu", "profile-cpu", "pcpu"   ],
                         help  = "Queries for CPU profiling information.",
                         exec  = execc                                   }
  where
    execc :: CmdExecFn
    execc pa _ ("start":p:_) = do
        let f = [("do", "start"), ("path", C8.pack p)]
        r <- httpResult (doPostRequest pa "/self/profile/cpu/" f) :: IO HttpResult
        putStrLn $ show r
    execc pa _ ("stop":_) = do
        let f = [("do", "stop"), ("path", "")]
        r <- httpResult (doPostRequest pa "/self/profile/cpu/" f) :: IO HttpResult
        putStrLn $ show r
    execc _ _ _ = hPutStrLn stderr $ "Expected a verb ('start' or 'stop') and a path to write profiling data to."

profilememc :: Command
profilememc =  Command { names = [ "profilemem", "profile-mem", "pmem"  ],
                         help  = "Queries for RAM profiling information.",
                         exec  = execc                                   }
  where
    execc :: CmdExecFn
    execc pa _ (p:_) = do
        let f = [("path", C8.pack p)]
        r <- httpResult (doPostRequest pa "/self/profile/mem/" f) :: IO HttpResult
        putStrLn $ show r
    execc _ _ _ = hPutStrLn stderr $ "Expected a path to write profiling data to."

seedleechc :: Command
seedleechc =  Command { names = [ "seedleech", "sl"                 ],
                        help  = "Sets the seed/leech info of a post.",
                        exec  = execc                                }
  where
    execc :: CmdExecFn
    execc pa _ (id:s:l:_) | isInt id && isInt s && isInt l = do
        let f = [("id", C8.pack id), ("seed", C8.pack s), ("leech", C8.pack l)]
        r <- httpResult (doPostRequest pa "/self/seedleech/" f) :: IO HttpResult
        putStrLn $ show r
    execc _ _ _ = hPutStrLn stderr $ "Expected a post ID, seed count and leech count."

