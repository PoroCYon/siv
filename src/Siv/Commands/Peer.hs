{-# LANGUAGE OverloadedStrings #-}
module Siv.Commands.Peer
    ( pingc
    , announcec
    , mirrorc
    , mirrorprogc
    , prsearchc
    , psearchc
    , precentc
    , ppopularc
    , pindexc
    ) where

import Data.Aeson (toJSON           )
import Data.List  (intercalate      )
import Data.Maybe (isJust           )
import Data.Text  (Text             )
import System.IO  (hPutStrLn, stderr)
import Text.Read  (readMaybe        )

import Siv.Address
import Siv.BaseByteString
import Siv.Command
import Siv.ResultTypes
import Siv.Util

import qualified Data.ByteString.Char8 as C8
import qualified Data.Text             as T

isPeer :: String -> Bool
isPeer ('Z':rest) = isJust (decodeBS $ C8.pack rest :: Maybe Base58ByteString)
isPeer   _        = False

pingc :: Command
pingc =  Command { names = [ "ping"      ],
                   help  = "Pings a peer.",
                   exec  = execc          }
  where
    execc :: CmdExecFn
    execc pa _ (pe:_) | isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/ping/") :: IO HttpResult
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address."

announcec :: Command
announcec =  Command { names = [ "announce", "ann"  ],
                       help  = "Announces to a peer.",
                       exec  = execc                 }
  where
    execc :: CmdExecFn
    execc pa _ (pe:_) | isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/announce/") :: IO HttpResult
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address."

mirrorc :: Command
mirrorc =  Command { names = [ "mirror", "mir" ],
                     help  = "Mirrors a peer."  ,
                     exec  = execc              }
  where
    execc :: CmdExecFn
    execc pa _ (pe:_) | isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/mirror/") :: IO HttpResult
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address."

mirrorprogc :: Command
mirrorprogc =  Command { names = [ "mprog", "mp"                            ],
                         help  = "Displays the progress of mirroring a peer.",
                         exec  = execc                                       }
  where
    execc :: CmdExecFn
    execc pa _ (pe:_) | isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/mirrorprogress/") :: IO (HttpRes Int)
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address."

-- TODO: unify with commands in Self.hs?

prsearchc :: Command
prsearchc =  Command { names = [ "prsearch", "prs"                 ],
                       help  = "Performs a remote search on a peer.",
                       exec  = execc                                }
  where
    execc :: CmdExecFn
    execc pa _ (pe:p:q:qs) | isJust (readMaybe p :: Maybe Int) && isPeer pe = do
        let f = [("query", C8.pack $ intercalate " " (q:qs)),
                 ("page" , C8.pack p                       )]
        r <- httpResult (doPostRequest pa ("/peer/" ++ pe ++ "/rsearch/") f) :: IO (HttpRes [ZifPost])
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address, page and search query."
psearchc :: Command
psearchc =  Command { names = [ "psearch", "psr"           ],
                      help  = "Performs a search on a peer.",
                      exec  = execc                         }
  where
    execc :: CmdExecFn
    execc pa _ (pe:p:q:qs) | isJust (readMaybe p :: Maybe Int) && isPeer pe = do
        let f = [("query", C8.pack $ intercalate " " (q:qs)),
                 ("page" , C8.pack p                       )]
        r <- httpResult (doPostRequest pa ("/peer/" ++ pe ++ "/search/") f) :: IO (HttpRes [ZifPost])
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address, page and search query."

precentc :: Command
precentc =  Command { names = [ "precentc", "prec"         ],
                      help  = "Lists a peer's recent posts.",
                      exec  = execc                         }
  where
    execc :: CmdExecFn
    execc pa _ (pe:p:_) | isJust (readMaybe p :: Maybe Int) && isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/recent/" ++ p ++ "/") :: IO (HttpRes [ZifPost])
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address and page number."

ppopularc :: Command
ppopularc =  Command { names = [ "ppopular", "pp"            ],
                       help  = "Lists a peer's popular posts.",
                       exec  = execc                          }
  where
    execc :: CmdExecFn
    execc pa _ (pe:p:_) | isJust (readMaybe p :: Maybe Int) && isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/popular/" ++ p ++ "/") :: IO (HttpRes [ZifPost])
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address and page number."

pindexc :: Command
pindexc =  Command { names = [ "pindex", "pind"            ],
                     help  = "Generates a peer's FTS index.",
                     exec  = execc                          }
  where
    execc :: CmdExecFn
    execc pa _ (pe:s:_) | isJust (readMaybe s :: Maybe Int) && isPeer pe = do
        r <- httpResult (doGetRequest pa $ "/peer/" ++ pe ++ "/index/" ++ s ++ "/") :: IO HttpResult
        putStrLn $ show r
    execc _  _  _ =
        hPutStrLn stderr $ "Expected peer address and integer."

