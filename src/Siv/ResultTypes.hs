{-# LANGUAGE OverloadedStrings, FlexibleInstances #-}
module Siv.ResultTypes (HttpResult, HttpRes(..), ZifPost(..), ZifEntry(..), SearchList(..)) where

import Data.Aeson          ((.:), (.=))
import Data.Aeson.Types    (FromJSON(..), ToJSON(..), Parser(..), Value(..))
import Data.ByteString     (ByteString)
import Data.HashMap.Strict (fromList  )
import Data.Text           (Text      )

import Siv.Address
import Siv.BaseByteString

import qualified Data.Text as T

data HttpRes a = Ok a
               | Err String
    deriving (Eq, Show)
type HttpResult = HttpRes ()

instance {-# OVERLAPS #-} FromJSON HttpResult where
    parseJSON (Object o) = o .: "status" >>= parseStatus
      where
        parseErr (String e) = return $ Err $ T.unpack e
        parseErr         _  = fail "Expected error string."
        parseStatus (String "err") = o .: "err" >>= parseErr
        parseStatus (String "ok" ) = return $ Ok ()
        parseStatus          _     = fail "Expected either 'err' or 'ok' as status."
    parseJSON         _  = fail "Expected a status object."
instance {-# OVERLAPS #-} ToJSON HttpResult where
    toJSON (Err e) = Object $ fromList $ [("status", String "err"), ("err", String $ T.pack e)]
    toJSON      _  = Object $ fromList $ [("status", String "ok" )]
instance FromJSON a => FromJSON (HttpRes a) where
    parseJSON (Object o) = o .: "status" >>= parseStatus
      where
        parseErr (String e) = return $ Err $ T.unpack e
        parseErr         j  = fail $ "Expected error string, but got " ++ show j
        parseStatus (String "err") =           o .: "err"   >>= parseErr
        parseStatus (String "ok" ) = fmap Ok $ o .: "value" >>= parseJSON
        parseStatus          j     = fail $ "Expected either 'err' or 'ok' as status, but got " ++ show j
    parseJSON _ = fail "Expected a status object."
instance ToJSON a => ToJSON (HttpRes a) where
    toJSON (Err e) = Object $ fromList $ [("status", String "err"), ("err", String $ T.pack e)]
    toJSON (Ok  v) = Object $ fromList $ [("status", String "ok" ), ("value", toJSON v)]

data ZifPost = ZifPost { postid     :: Int    ,
                         infoHash   :: Text   ,
                         title      :: Text   ,
                         size       :: Int    ,
                         fileCount  :: Int    ,
                         seeders    :: Int    ,
                         leechers   :: Int    ,
                         uploadDate :: Integer,
                         tags       :: Text   }
    deriving (Eq, Show)

instance FromJSON ZifPost where
    parseJSON (Object o) = do
        id <- o .: "Id"         >>= parseJSON :: Parser Int
        ih <- o .: "InfoHash"   >>= parseJSON :: Parser Text
        tt <- o .: "Title"      >>= parseJSON :: Parser Text
        sz <- o .: "Size"       >>= parseJSON :: Parser Int
        fc <- o .: "FileCount"  >>= parseJSON :: Parser Int
        ss <- o .: "Seeders"    >>= parseJSON :: Parser Int
        ls <- o .: "Leechers"   >>= parseJSON :: Parser Int
        ud <- o .: "UploadDate" >>= parseJSON :: Parser Integer
        ts <- o .: "Tags"       >>= parseJSON :: Parser Text
        return $ ZifPost { postid = id, infoHash = ih, title = tt, size = sz,
                           fileCount = fc, seeders = ss, leechers = ls,
                           uploadDate = ud, tags = ts }
    parseJSON  j = fail $ "Expected a post object, but got " ++ show j
instance ToJSON ZifPost where
    toJSON p =
        Object $ fromList $ [("Id"        , toJSON $ postid     p),
                             ("InfoHash"  , toJSON $ infoHash   p),
                             ("Title"     , toJSON $ title      p),
                             ("Size"      , toJSON $ size       p),
                             ("FileCount" , toJSON $ fileCount  p),
                             ("Seeders"   , toJSON $ seeders    p),
                             ("Leechers"  , toJSON $ leechers   p),
                             ("UploadDate", toJSON $ uploadDate p),
                             ("Tags"      , toJSON $ tags       p)]

data ZifEntry = ZifEntry { address   :: ZifAddress      ,
                           name      :: Text            ,
                           descr     :: Text            ,
                           pubAddr   :: PubAddress      ,
                           publicKey :: Base64ByteString,
                           postCount :: Int             ,
                           signature :: Base64ByteString}
    deriving (Eq, Show)

instance FromJSON ZifEntry where
    parseJSON (Object o) = do
        za <- o .: "address"       >>= parseJSON :: Parser ZifAddress
        nm <- o .: "name"          >>= parseJSON :: Parser Text
        ds <- o .: "desc"          >>= parseJSON :: Parser Text
        pa <- o .: "publicAddress" >>= parseJSON :: Parser Text
        pk <- o .: "publicKey"     >>= parseJSON :: Parser Base64ByteString
        pc <- o .: "postCount"     >>= parseJSON :: Parser Int
        -- updated :: Int
        sg <- o .: "signature"     >>= parseJSON :: Parser Base64ByteString
        -- collectionHash :: ?
        pt <- o .: "port"          >>= parseJSON :: Parser Int
        -- seeds :: ?
        -- seeding :: ?
        -- seed :: Int
        return $ ZifEntry { address = za, name = nm, descr = ds, postCount = pc,
                            publicKey = pk, signature = sg,
                            pubAddr = PubAddress { host = T.unpack pa, port = pt } }
    parseJSON j = fail $ "Expected a local peer object, but got " ++ show j
instance ToJSON ZifEntry where
    toJSON p =
        Object $ fromList $ [("address"      , toJSON $ address    p),
                             ("name"         , toJSON $ name       p),
                             ("desc"         , toJSON $ descr      p),
                             ("publicKey"    , toJSON $ publicKey  p),
                             ("postCount"    , toJSON $ postCount  p),
                             ("signature"    , toJSON $ signature  p),
                             ("publicAddress", toJSON $ host $ pubAddr p),
                             ("port"         , toJSON $ port $ pubAddr p)]

data SearchList = SearchList { source :: ZifAddress ,
                               posts  :: [ZifPost]  }
    deriving (Eq, Show)

instance FromJSON SearchList where
    parseJSON (Object o) = do
        sr <- o .: "source" >>= parseJSON :: Parser String
        ps <- o .: "posts"  >>= parseJSON :: Parser [ZifPost]
        return $ SearchList { source = read sr, posts = ps }
    parseJSON j = fail $ "Expected a remote peer object, but got " ++ show j

instance ToJSON SearchList where
    toJSON sl = Object $ fromList $ [("source", toJSON $ source sl),
                                     ("posts" , toJSON $ posts  sl)]

