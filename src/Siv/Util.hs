{-# LANGUAGE OverloadedStrings #-}
module Siv.Util
    ( doGetRequest
    , doPostRequest
    , httpResult
    , isHost
    , encodeJSON
    ) where

import Network.HTTP.Simple
import Data.Aeson
import Data.Aeson.Types    (FromJSON(..), Value(..))
import Data.ByteString     (ByteString)

import Siv.Address
import Siv.ResultTypes

import qualified Data.ByteString.Char8 as C8
import qualified Data.ByteString.Lazy  as LBS

doGetRequest :: PubAddress -> String -> Request
doGetRequest a p = setRequestMethod "GET"
                 $ setRequestPath (C8.pack p)
                 $ setRequestHost (C8.pack $ host a)
                 $ setRequestPort (port a)
                 $ defaultRequest

doPostRequest :: PubAddress -> String -> [(ByteString, ByteString)] -> Request
doPostRequest a p f = setRequestMethod "POST"
                    $ setRequestPath (C8.pack p)
                    $ setRequestHost (C8.pack $ host a)
                    $ setRequestPort (port a)
                    $ setRequestBodyURLEncoded f
                    $ defaultRequest

httpResult :: FromJSON a => Request -> IO a
httpResult r = fmap getResponseBody $ httpJSON r

isHost :: String -> Bool
isHost a = not $ elem '/' a

encodeJSON :: Value -> ByteString
encodeJSON j = LBS.toStrict $ encode j

